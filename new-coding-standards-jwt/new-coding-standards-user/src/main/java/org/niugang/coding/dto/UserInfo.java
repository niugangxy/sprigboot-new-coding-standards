package org.niugang.coding.dto;

import lombok.*;

/**
 * 用户信息对象
 *
 * @author Created by niugang on 2019/01/11/15:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UserInfo {
    private Long id;
    private String name;
}
