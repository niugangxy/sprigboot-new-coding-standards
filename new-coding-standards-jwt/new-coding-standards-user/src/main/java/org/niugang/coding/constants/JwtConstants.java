package org.niugang.coding.constants;

/**
 * jwt需要加密的信息
 *
 * @author Created by niugang on 2019/01/11/15:43
 */
public class JwtConstants {
    public static final String JWT_KEY_ID = "id";
    public static final String JWT_KEY_USER_NAME = "username";
}
