package org.niugang.coding.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 数据传输对象
 *
 * @author Created by niugang on 2018/12/26/13:43
 */
@ToString
@Data
@EqualsAndHashCode
@ApiModel(description="用户信息参数对象")
public class UserDTO {

    @ApiModelProperty(value = "ID",  notes = "添加不同传id")
    private Integer id;
    /**
     * {@code @NotNull javax.validation 的包 }
     */
    @ApiModelProperty(value = "账号")
    @NotNull
    private String account;
    @ApiModelProperty(value = "姓名")
    @NotNull
    private String name;
    @NotNull
    @ApiModelProperty(value = "性别")
    private String sex;
    @NotNull
    @Pattern(regexp = "^[0-9]{11}$")
    @ApiModelProperty(value = "电话")
    private String phone;
    @NotNull
    @Email
    @ApiModelProperty(value = "邮箱")
    private String email;
}
