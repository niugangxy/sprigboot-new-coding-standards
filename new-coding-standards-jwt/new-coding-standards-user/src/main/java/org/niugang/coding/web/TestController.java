package org.niugang.coding.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试类
 *
 * @author Created by niugang on 2018/12/26/11:58
 */
@RestController
@RequestMapping("test")
@Api(value = "测试接口", tags = {"测试操作接口"})
public class TestController {

    @ApiOperation(value = "项目测试", httpMethod = "GET")
    @GetMapping
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("hello world");
    }


}
