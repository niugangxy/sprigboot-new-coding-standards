package org.niugang.coding.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

/**
 * @author Created by niugang on 2018/12/26/13:40
 */
@Data
@ToString
@EqualsAndHashCode
public class User {
	private Integer id;
	private String account;
	private String name;
	private String sex;
	private String phone;
	private String email;
	private String quanPin;
	private String jianPin;
	private String ip;
	private Integer deptId;
	private String roleId;
	private String idCard;
	private String fileUrl;
	private String password;
	private String deleteFlag;
	private Date createTime;
	private String creator;
	private Date updateTime;
	private String updator;
	private Long version;

	

}
