package org.niugang.coding.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.niugang.coding.constants.UserStringEnum;
import org.niugang.coding.dto.LoginDTO;
import org.niugang.coding.dto.UserInfo;
import org.niugang.coding.oauth.JwtProperties;
import org.niugang.coding.utils.JwtUtils;
import org.niugang.coding.vo.UserVO;
import org.niugang.coding.dto.UserDTO;
import org.niugang.coding.mapper.UserMapper;
import org.niugang.coding.pojo.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author Created by niugang on 2018/12/26/16:10
 */
@Service
public class UserService implements BaseService<UserDTO, UserVO> {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private JwtProperties jwtProperties;


    @Override
    public List<UserVO> getList(UserDTO userDTO) {
        return null;
    }

    @Override
    public PageInfo<UserVO> getListByPage(UserDTO userDTO, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        if (userDTO == null) {
            userDTO = new UserDTO();
        }
        List<User> listByPage = userMapper.getListByPage(userDTO);
        if (listByPage != null && listByPage.size() > 0) {
            List<UserVO> userVOS = new ArrayList<>(listByPage.size());
            for (User user : listByPage) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(user, userVO);
                userVOS.add(userVO);
            }
            int count = userMapper.count(userDTO);
            PageInfo<UserVO> pageInfo = new PageInfo<>(userVOS);
            pageInfo.setTotal(count);
            return pageInfo;
        } else {
            PageInfo<UserVO> pageInfo = new PageInfo<>(Collections.emptyList());
            return pageInfo;
        }

    }

    @Override
    public void insert(UserDTO s) {
        User user = new User();
        BeanUtils.copyProperties(s, user);
        user.setDeleteFlag(UserStringEnum.DELETE_FLAG.getValue());
        userMapper.insert(user);
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public UserVO getById(int id) {
        return null;
    }

    @Override
    public UserVO get(UserDTO userDTO) {
        return null;
    }

    @Override
    public int count(UserDTO userDTO) {
        return 0;
    }

    @Override
    public void update(UserDTO userDTO) {

    }


    public String checkUserInfo(LoginDTO loginDTO) {
        long l = System.currentTimeMillis();
        UserInfo userInfo = new UserInfo(l, loginDTO.getAccount());
        //生成Token
        String token = JwtUtils.generateToken(userInfo, jwtProperties.getPrivateKey(), jwtProperties.getExpire());
        return token;

    }
}
