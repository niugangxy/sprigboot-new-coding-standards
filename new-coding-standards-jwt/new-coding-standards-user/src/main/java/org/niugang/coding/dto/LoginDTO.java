package org.niugang.coding.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 *
 *
 * @author Created by niugang on 2019/1/11/15:55
 */
@ToString
@Data
@EqualsAndHashCode
@ApiModel
public class LoginDTO {

    @ApiModelProperty(value = "账号")
    @NotNull
    private String account;

    @ApiModelProperty(value = "密码")
    @NotNull
    private String password;
}
