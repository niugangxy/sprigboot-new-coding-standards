const serviceModule = {
    // course
    getRelatedCourseCategoryTreeList:{
        url: '/course/courseCategoryController/getRelatedCourseCategoryTreeList.do',
        method: 'get'
    },
    getCourseCateogryTree: {
        url: '/course/courseCategoryController/getCourseCateogryTreeList.do',
        method: 'get'
    },
    getIncludeSubCourseCateogryTreeList: {
        url: '/course/courseCategoryController/getIncludeSubCourseCateogryTreeList.do',
        method: 'get'
    },
    editCourseCateogryTree: {
        url: '/course/courseCategoryController/updateCourseCategory.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addCourseCateogryTree: {
        url: '/course/courseCategoryController/addCourseCategory.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    delCourseCateogryTree: {
        url: '/course/courseCategoryController/batchDelete.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    // course table
    queryCourse:{
        url: '/course/courseController/queryCourse.do',
        method: 'get'
    },
    batchDeleteCourse:{
        url: '/course/courseController/batchDeleteCourse.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addCourse:{
        url: '/course/courseController/addCourse.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addAndPublishCourse:{
        url: '/course/courseController/addAndPublishCourse.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    publishCourse:{
        url: '/course/courseController/publishCourse.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getCourseDetail:{
        url: '/course/courseController/getCourseDetail.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getCourseCategoryRelateCourseList:{
        url: '/course/courseCategoryController/getCourseCategoryRelateCourseList.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    clientApi:{
        url: '/client/clientApi.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getAccountInfo:{
        url: '/rpc/MotanRpcClientController/getAccountInfo.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getAutenticationToken:{
        url: '/autentication/UserAutenticationController/getAutenticationToken.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    // 题库
    queryQuestionRepo:{
        url: '/questionRepo/questionRepoController/queryQuestionRepo.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addQuestionRepo:{
        url: '/questionRepo/questionRepoController/addQuestionRepo.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    batchDeleteQuestionRepo:{
        url: '/questionRepo/questionRepoController/batchDeleteQuestionRepo.do',
            method: 'post',
            header:{
            "Accept": "application/json",
                "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getCourseList:{
        url: '/course/courseController/getCourseList.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addTopic:{
        url: '/topic/topicController/addTopic.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    updateQuestionRepRelativeInfo:{
        url: '/questionRepo/questionRepoController/updateQuestionRepRelativeInfo.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    deleteTopic:{
        url: '/topic/topicController/deleteTopic.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    updateQuestionRepo:{
        url: '/questionRepo/questionRepoController/updateQuestionRepo.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    updateQuestionRepRelativeInfo:{
        url: '/questionRepo/questionRepoController/updateQuestionRepRelativeInfo.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    updateTopic:{
        url: '/topic/topicController/updateTopic.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    publishQuestionRepo:{
        url: '/questionRepo /questionRepoController/publishQuestionRepo.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    queryQuestionRepoDetail:{
        url: '/questionRepo/questionRepoController/queryQuestionRepoDetail.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    findPaperList:{
        url: '/paper/paperController/findPaperList.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    // paper
    getQuestionRepoList:{
        url: '/questionRepo/questionRepoController/getQuestionRepoList.do',
        method: 'get',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    closePaper:{
        url: '/paper/paperController/closePaper.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    addRandomPaper:{
        url: '/paper/paperController/addRandomPaper.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    getPaperInfo:{
        url: '/paper/paperController/getPaperInfo.do',
        method: 'post',
        header:{
            // "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    publishPaper:{
        url: '/paper/paperController/publishPaper.do',
        method: 'post',
        header:{
            // "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
            // "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    // dept
    updateOADeptInfo:{
        url: '/user/deptInfoController/updateOADeptInfo.do',
        method: 'post',
        header:{
            // "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
            // "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    findDeptTree:{
        url: '/user/deptInfoController/findDeptTree.do',
        method: 'post',
        header:{
            // "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    findDeptList:{
        url: '/user/deptInfoController/findDeptList.do',
        method: 'post',
        header:{
            // "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    getFileServerUrl:{
        url: '/edums/rpc/MotanRPcClientConTroller/getFileServerUrl.do',
        method: 'post',
        header:{
            "Accept": "application/json",
            "Content-Type": "application/json; charset=UTF-8"
        }
    },
    logList:{
        url: '/users/list',
        method: 'get',
        header:{
            "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    login:{
        url: '/login',
        method: 'post',
        header:{
            "Content-Type": "application/x-www-form-urlencoded"
        }
    },
    registList:{
        url: '/api/user/list',
        method: 'post',
        header:{
            "Content-Type": "application/x-www-form-urlencoded"
        }
    }



}
const serveceApi =  serviceModule;

export default serveceApi
