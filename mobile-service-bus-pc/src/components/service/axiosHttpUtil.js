import axios from 'axios'
import qs from 'qs'
import Vue from "vue";

axios.interceptors.request.use(config => {
    return config
}, error => {
    return Promise.reject(error)
})


axios.interceptors.response.use(response => {
    return response
}, error => {
    return Promise.resolve(error.response)
})

function errorState(response) {
    console.log(response)
    // 如果http状态码正常，则直接返回数据
    if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
        return response
        // 如果不需要除了data之外的数据，可以直接 return response.data
    }else{
        Vue.prototype.$alert('网络异常', '提示', {
            confirmButtonText: '确定'
        });
    }

}

function successState(res) {
    // store.commit('UPDATE_LOADING',false) //隐藏loading
    //统一判断后端返回的错误码
    if(res.data.errCode == '000002'){
        Vue.prototype.$msg.alert.show({
            title: '提示',
            content: res.data.errDesc||'网络异常',
            onShow () {
            },
            onHide () {
                console.log('确定')
            }
        })
    }else if(res.data.errCode != '000002'&&res.data.errCode != '000000') {
        Vue.prototype.$msg.alert.show({
            title: '提示',
            content: res.data.errDesc||'网络异常',
            onShow () {

            },
            onHide () {
                console.log('确定')
            }
        })
    }
}

function isParamsValid(params) {
    return params.method && params.url ? true : false;
}
const axiosHttpUtil = (opts, data) => {

    let Public = { //公共参数
        // '': ""
    }
    // dev
    // let baseURL = 'api/edums/';
    // test
    // let baseURL = '/edums/';
    // pro
    let baseURL = '../api/';

    // valid params is null有
    if(!isParamsValid(opts)){
        return false;
    }

    let defaultHeader = opts.header ? opts.header : ( opts.method =='get'?{
        'X-Requested-With': 'XMLHttpRequest',
        "Accept": "application/json",
        "Content-Type": "application/json; charset=UTF-8",
    }:{
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json;charset=UTF-8'
    } );

    // defaultHeader.token = sessionStorage.getItem('online_token');
    // defaultHeader.appId = sessionStorage.getItem('online_appId');
    // defaultHeader.companyId = sessionStorage.getItem('online_companyId');

    var params_test = '';
    if( defaultHeader && defaultHeader['Content-Type'] === 'application/x-www-form-urlencoded'){
        params_test = qs.stringify(  data );
    }else {
        params_test = data;
    }
    let httpDefaultOpts = { //http默认配置
        method: opts.method ,
        baseURL,
        url: opts.url,
        timeout: 10000,
        params:  Object.assign(Public, data),
        // data: qs.stringify(Object.assign(Public, data)),
        data: params_test,
        headers: defaultHeader
    }

    if(opts.method=='get'){
        delete httpDefaultOpts.data
    }else{
        delete httpDefaultOpts.params
    }

    let promise = new Promise(function(resolve, reject) {
        axios(httpDefaultOpts).then(
            (res) => {
                if( res || res === 0 ){
                    res = res;
                }else{
                    res = {
                        data : {
                            rtnMsg :  '网络连接失败，请稍后重试'
                        }
                    }
                }
                resolve(res)
            }
        ).catch(
            (response) => {
                reject(response)
            }
        )

    })
    return promise;
}

export default axiosHttpUtil
