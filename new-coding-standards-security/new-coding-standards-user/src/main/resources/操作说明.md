- http://localhost:8083/login?username=110110&password=123456

````

{
    "data": {
        "account": "110110",
        "createTime": 1535107469000,
        "deleteFlag": "1",
        "email": "863263957@qq.com",
        "id": 3,
        "jianPin": "wbb",
        "phone": "13060401016",
        "quanPin": "wangbobo",
        "sex": "1",
        "username": "王勃勃",
        "version": 180824184424
    },
    "message": "登录成功",
    "path": "/login",
    "status": 200,
    "timestamp": 1548420176563
}


- http://localhost:8083/users/list

```json
{
    "message": "权限不足，请联系管理员",
    "path": "http://localhost:8083/users/list",
    "status": 401,
    "timestamp": 1548420182478
}
```

http://localhost:8083/users/finds

```json
{
    "total": 20,
    "totalPage": 1,
    "data": [
        {
            "id": 1,
            "account": "wenpeng123",
            "username": "问鹏",
            "sex": "1",
            "phone": "5094037891"
        },
        {
            "id": 3,
            "account": "110110",
            "username": "王勃勃",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 4,
            "account": "110111",
            "username": "石磊",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 34,
            "account": "110113",
            "username": "牛刚刚",
            "sex": "1",
            "phone": "15094031789"
        },
        {
            "id": 35,
            "account": "110113",
            "username": "李龙1",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 36,
            "account": "110113",
            "username": "211",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 37,
            "account": "110116",
            "username": "4455",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 38,
            "account": "110114",
            "username": "56262",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 39,
            "account": "110115",
            "username": "498985",
            "sex": "1",
            "phone": "13060401016"
        },
        {
            "id": 40,
            "account": "1234",
            "username": "fwfewefwfe",
            "sex": "1",
            "phone": "15094037891"
        }
    ],
    "pageSize": 10,
    "pageNO": 1
}
```