/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.17-log : Database - bootdb
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bootdb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `bootdb`;

/*Table structure for table `t_boot_admin` */

DROP TABLE IF EXISTS `t_boot_admin`;

CREATE TABLE `t_boot_admin` (
  `c_id` char(32) NOT NULL COMMENT '主键',
  `c_account` varchar(64) DEFAULT NULL COMMENT '账号',
  `c_phone` char(11) DEFAULT NULL COMMENT '电话',
  `c_email` varchar(32) DEFAULT NULL COMMENT '邮箱',
  `c_jianpin` varchar(64) DEFAULT NULL COMMENT '简拼',
  `c_quanpin` varchar(64) DEFAULT NULL COMMENT '全拼',
  `c_deleteflag` char(1) DEFAULT NULL COMMENT '删除标识',
  `c_createtime` datetime DEFAULT NULL,
  `c_creator` varchar(32) DEFAULT NULL,
  `c_updatetime` datetime DEFAULT NULL,
  `c_updator` varchar(64) DEFAULT NULL,
  `c_name` varchar(64) DEFAULT NULL,
  `c_password` varchar(64) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

/*Data for the table `t_boot_admin` */

insert  into `t_boot_admin`(`c_id`,`c_account`,`c_phone`,`c_email`,`c_jianpin`,`c_quanpin`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_name`,`c_password`) values ('1','admin','15094037891','863263957@qq.com','superadmin','sd','1','2018-07-02 21:10:11',NULL,NULL,NULL,'admin','$2a$10$gNjAM6Uj8DkZD3ua/pGYJem/spU9d6gTW6/kRQztNrHRoR1yEUhLC');
insert  into `t_boot_admin`(`c_id`,`c_account`,`c_phone`,`c_email`,`c_jianpin`,`c_quanpin`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_name`,`c_password`) values ('4028aa206465702201646571417b0000','niugang','15094037891','863263957@qq.com','ng','niugang','1','2018-07-04 21:17:58',NULL,NULL,NULL,'牛刚','$2a$10$gNjAM6Uj8DkZD3ua/pGYJem/spU9d6gTW6/kRQztNrHRoR1yEUhLC');
insert  into `t_boot_admin`(`c_id`,`c_account`,`c_phone`,`c_email`,`c_jianpin`,`c_quanpin`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_name`,`c_password`) values ('4028aa206465702201646574533a0002','wangbobo','15094037891','788944@qq.com','wbb','wangbobo','1','2018-07-04 21:21:20',NULL,NULL,NULL,'王勃勃','$2a$10$gNjAM6Uj8DkZD3ua/pGYJem/spU9d6gTW6/kRQztNrHRoR1yEUhLC');

/*Table structure for table `t_boot_admin_role` */

DROP TABLE IF EXISTS `t_boot_admin_role`;

CREATE TABLE `t_boot_admin_role` (
  `c_id` char(32) DEFAULT NULL,
  `c_admin_id` char(32) DEFAULT NULL COMMENT '管理员id',
  `c_role_id` char(32) DEFAULT NULL COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_boot_admin_role` */

insert  into `t_boot_admin_role`(`c_id`,`c_admin_id`,`c_role_id`) values ('1','1','1');
insert  into `t_boot_admin_role`(`c_id`,`c_admin_id`,`c_role_id`) values ('4028aa20646570220164657141aa0001','4028aa206465702201646571417b0000','4028aa20646482630164649acc170006');
insert  into `t_boot_admin_role`(`c_id`,`c_admin_id`,`c_role_id`) values ('4028aa206465702201646574537d0003','4028aa206465702201646574533a0002','4028aa20646482630164649acc170006');

/*Table structure for table `t_boot_dept` */

DROP TABLE IF EXISTS `t_boot_dept`;

CREATE TABLE `t_boot_dept` (
  `c_id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `c_dept_name` varchar(128) DEFAULT NULL COMMENT '部门名称',
  `c_parent_id` int(11) DEFAULT NULL COMMENT '父id',
  `c_code` varchar(128) DEFAULT NULL COMMENT '编码',
  `c_delete_flag` char(1) DEFAULT NULL COMMENT '删除标识 0删除 1启用',
  `c_quanpin` varchar(128) DEFAULT NULL COMMENT '全拼',
  `c_jianpin` varchar(64) DEFAULT NULL COMMENT '简拼',
  `c_createtime` datetime DEFAULT NULL,
  `c_creator` varchar(64) DEFAULT NULL,
  `c_updatetime` datetime DEFAULT NULL,
  `c_updator` varchar(64) DEFAULT NULL,
  `c_version` bigint(20) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='部门表';

/*Data for the table `t_boot_dept` */

insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (3,'新大捷安',0,NULL,'1','xindajiean','xdja','2018-07-05 20:43:26',NULL,NULL,NULL,180705204326);
insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (4,'西安研究院',3,'3#4','1','xianyanjiuyuan','xayjy','2018-07-06 15:23:58',NULL,NULL,NULL,180706152358);
insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (10,'测试组',3,'3#10','1','ceshizu','csz','2018-07-06 16:02:22',NULL,'2018-07-06 16:02:22',NULL,180706160222);
insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (16,'测试组1',10,'3#10#16','1','ceshizu1','csz1','2018-07-06 16:51:38',NULL,'2018-07-06 16:51:38',NULL,180706165138);
insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (17,'测试组1',10,'3#10#17','1','ceshizu1','csz1','2018-07-06 17:03:30',NULL,'2018-07-06 17:03:30',NULL,180706170330);
insert  into `t_boot_dept`(`c_id`,`c_dept_name`,`c_parent_id`,`c_code`,`c_delete_flag`,`c_quanpin`,`c_jianpin`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (18,'测试组11',17,'3#10#17#18','1','ceshizu11','csz11','2018-07-06 17:04:57',NULL,'2018-07-06 17:05:16',NULL,180706170514);

/*Table structure for table `t_boot_menu` */

DROP TABLE IF EXISTS `t_boot_menu`;

CREATE TABLE `t_boot_menu` (
  `c_id` char(32) NOT NULL COMMENT '主键',
  `c_url` varchar(128) DEFAULT NULL COMMENT '访问路径',
  `c_name` varchar(128) DEFAULT NULL COMMENT '菜单名称',
  `c_ischildren` char(1) DEFAULT NULL COMMENT '是否为子     1是 0不是',
  `c_parent` char(32) DEFAULT NULL COMMENT '父路径id',
  `c_deleteflag` char(1) DEFAULT '1' COMMENT '删除标识   1启动 0 删除',
  `c_createtime` datetime DEFAULT NULL,
  `c_creator` varchar(64) DEFAULT NULL,
  `c_updatetime` datetime DEFAULT NULL,
  `c_updator` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COMMENT='系统菜单表';

/*Data for the table `t_boot_menu` */

insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645a3fb701645a3fe6a60000','','系统日志','0','0','1','2018-07-02 17:08:15','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645a562f01645a56590a0000','/log/list','日志列表','1','4028aa9c645a3fb701645a3fe6a60000','1','2018-07-02 17:32:46','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645a831201645a833bee0000',NULL,'用户','0','0','1','2018-07-02 18:21:47','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645a847d01645a84a6bf0000','/administrators/list','后台管理员','1','4028aa9c645a831201645a833bee0000','1','2018-07-02 18:23:20','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645af1ff01645af2282a0000',NULL,'系统设置','0','0','1','2018-07-02 20:22:57','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645af3dd01645af406710000','/menu/list','菜单列表','1','4028aa9c645af1ff01645af2282a0000','1','2018-07-02 20:24:59','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645afa7401645afa9e320000','/role/list','角色管理','1','4028aa9c645a831201645a833bee0000','1','2018-07-02 20:32:11','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645e5a8101645e5ac3500000',NULL,'人事行政','0','0','1','2018-07-03 12:16:04','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aa9c645e5beb01645e5c22660000','/employee/list','员工考勤','1','4028aa9c645e5a8101645e5ac3500000','1','2018-07-03 12:17:34','admin',NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aaf6646833600164685014010000','/employee/personalInfo','个人考勤','1','4028aa9c645e5a8101645e5ac3500000','1','2018-07-05 10:40:36',NULL,NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('4028aaf6646a82a201646aadf3490000','/user/list','注册用户','1','4028aa9c645a831201645a833bee0000','1','2018-07-05 21:42:22',NULL,NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('8b8cf01f64606de901646071c0eb0000','','我的设置','0','0','1','2018-07-03 22:00:25',NULL,NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('8b8cf01f646079730164607b6d000000','/set/modifyPassword','修改密码','1','8b8cf01f64606de901646071c0eb0000','1','2018-07-03 22:10:59',NULL,NULL,NULL);
insert  into `t_boot_menu`(`c_id`,`c_url`,`c_name`,`c_ischildren`,`c_parent`,`c_deleteflag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`) values ('8b8cf01f646079730164607ef7800001','/set/userInfo','基本信息','1','8b8cf01f64606de901646071c0eb0000','1','2018-07-03 22:14:51',NULL,NULL,NULL);

/*Table structure for table `t_boot_permission` */

DROP TABLE IF EXISTS `t_boot_permission`;

CREATE TABLE `t_boot_permission` (
  `c_id` char(32) DEFAULT NULL,
  `c_role_id` char(32) DEFAULT NULL COMMENT '角色id',
  `c_menu_id` char(32) DEFAULT NULL COMMENT '菜单id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限表';

/*Data for the table `t_boot_permission` */

insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('1','1','4028aa9c645a3fb701645a3fe6a60000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('2','1','4028aa9c645af1ff01645af2282a0000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('3','1','4028aa9c645a831201645a833bee0000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4028aa20646482630164649862bf0004','4028aa20646482630164649862800003','4028aa9c645e5a8101645e5ac3500000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4028aa206464826301646498632c0005','4028aa20646482630164649862800003','8b8cf01f64606de901646071c0eb0000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4028aa20646482630164649acc4d0007','4028aa20646482630164649acc170006','4028aa9c645a3fb701645a3fe6a60000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4028aa20646482630164649accad0008','4028aa20646482630164649acc170006','4028aa9c645e5a8101645e5ac3500000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4028aa20646482630164649acce40009','4028aa20646482630164649acc170006','8b8cf01f64606de901646071c0eb0000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('4','1','4028aa9c645e5a8101645e5ac3500000');
insert  into `t_boot_permission`(`c_id`,`c_role_id`,`c_menu_id`) values ('5','1','8b8cf01f64606de901646071c0eb0000');

/*Table structure for table `t_boot_role` */

DROP TABLE IF EXISTS `t_boot_role`;

CREATE TABLE `t_boot_role` (
  `c_id` char(32) NOT NULL COMMENT '主键',
  `c_role_nicename` varchar(64) DEFAULT NULL COMMENT '角色昵称',
  `c_role_description` varchar(128) DEFAULT NULL COMMENT '角色描述',
  `c_role_name` varchar(64) DEFAULT NULL COMMENT '角色名',
  `c_delete_flag` char(1) DEFAULT '1' COMMENT '删除标识  1启动 0删除',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `t_boot_role` */

insert  into `t_boot_role`(`c_id`,`c_role_nicename`,`c_role_description`,`c_role_name`,`c_delete_flag`) values ('1','superadmin','权限最大','超级管理员','1');
insert  into `t_boot_role`(`c_id`,`c_role_nicename`,`c_role_description`,`c_role_name`,`c_delete_flag`) values ('4028aa20646482630164649862800003','putongyonghu','普通用户权限','普通用户','1');
insert  into `t_boot_role`(`c_id`,`c_role_nicename`,`c_role_description`,`c_role_name`,`c_delete_flag`) values ('4028aa20646482630164649acc170006','putongguanliyuan','普通管理员拥有的权限','普通管理员','1');

/*Table structure for table `t_boot_user` */

DROP TABLE IF EXISTS `t_boot_user`;

CREATE TABLE `t_boot_user` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `c_account` varchar(64) DEFAULT NULL COMMENT '账号',
  `c_name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `c_sex` char(1) DEFAULT NULL COMMENT '性别  1男 2女',
  `c_phone` varchar(64) DEFAULT NULL COMMENT '电话',
  `c_email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `c_quanpin` varchar(128) DEFAULT NULL COMMENT '全拼',
  `c_jianpin` varchar(64) DEFAULT NULL COMMENT '简拼',
  `c_ip` varchar(32) DEFAULT NULL COMMENT 'ip地址',
  `c_dept_id` int(11) DEFAULT NULL COMMENT '部门id',
  `c_role_id` char(32) DEFAULT NULL COMMENT '角色id',
  `c_idcard` varchar(32) DEFAULT NULL COMMENT '身份证',
  `c_file_url` varchar(64) DEFAULT NULL COMMENT '头像',
  `c_password` varchar(64) DEFAULT NULL COMMENT '密码',
  `c_delete_flag` char(1) DEFAULT NULL COMMENT '删除标识',
  `c_createtime` datetime DEFAULT NULL,
  `c_creator` varchar(64) DEFAULT NULL,
  `c_updatetime` datetime DEFAULT NULL,
  `c_updator` varchar(64) DEFAULT NULL,
  `c_version` bigint(20) DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='用户表';

/*Data for the table `t_boot_user` */

insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (1,'wenpeng123','问鹏','1','5094037891','863263957@qq.com','wenpeng','wp','11.12.112.159',3,'4028aa20646482630164649862800003',NULL,'','$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-07-06 12:00:57',NULL,NULL,NULL,180706120057);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (3,'110110','王勃勃','1','13060401016','863263957@qq.com','wangbobo','wbb',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-08-24 18:44:29',NULL,NULL,NULL,180824184424);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (4,'110111','石磊','1','13060401016','863263957@qq.com','shilei','sl',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-08-24 18:51:41',NULL,NULL,NULL,180824185139);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (34,'110113','牛刚刚','1','15094031789','1235656@qq.com','lilong','ll',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','','2018-08-24 19:55:01',NULL,'2018-08-24 21:29:04',NULL,180824195501);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (35,'110113','李龙1','1','13060401016','863263957@qq.com','lilong','ll',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-11-04 17:01:26',NULL,NULL,NULL,181104170126);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (36,'110113','211','1','13060401016','863263957@qq.com','lilong','ll',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-11-04 17:02:22',NULL,NULL,NULL,181104170222);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (37,'110116','4455','1','13060401016','863263957@qq.com','lilong','ll',NULL,NULL,NULL,NULL,NULL,'$2a$10$MVcmpGD0eLuk/4N9LXR6f.0DYv.Nbb2S.VQ/6nYKEqXNZEwYtndOy','1','2018-11-04 17:03:17',NULL,NULL,NULL,181104170317);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (38,'110114','56262','1','13060401016','863263957@qq.com','lilong','892',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (39,'110115','498985','1','13060401016','863263957@qq.com','lilong','233',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (40,'1234','fwfewefwfe','1','15094037891','86363627@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2018-12-26 21:46:04',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (41,'1234','fwfewefwfe','1','15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2018-12-27 09:32:31',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (42,'1234','fwfewefwfe',NULL,'15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:10:49',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (43,'1234','fwfewefwfe',NULL,'15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:12:22',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (44,'1234','fwfewefwfe',NULL,'15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:13:05',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (45,'1234','fwfewefwfe',NULL,'15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:15:03',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (46,'1234','fwfewefwfe',NULL,'15094037891',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:15:34',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (47,'123454','6846sasd','1','15094037891','1234@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:36:29',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (48,'123454','6846sasd','1','15094037891','1234@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:38:14',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (49,'123454','6846sasd','1','15094037891','1234@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:45:02',NULL,NULL,NULL,NULL);
insert  into `t_boot_user`(`c_id`,`c_account`,`c_name`,`c_sex`,`c_phone`,`c_email`,`c_quanpin`,`c_jianpin`,`c_ip`,`c_dept_id`,`c_role_id`,`c_idcard`,`c_file_url`,`c_password`,`c_delete_flag`,`c_createtime`,`c_creator`,`c_updatetime`,`c_updator`,`c_version`) values (50,'123454','6846sasd','1','15094037891','1234@qq.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2019-01-18 16:45:23',NULL,NULL,NULL,NULL);

