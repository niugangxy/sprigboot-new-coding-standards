package org.niugang.coding.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.niugang.coding.exception.UsernameNotNullException;
import org.niugang.coding.pojo.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * 授权认证业务类
 *
 * @author niugang UserDetailsService spring security包里面的
 * 重写loadUserByUsername方法
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    // UserService自定义的，从数据查询信息
    @Resource
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username == null) {
            throw new UsernameNotNullException();
        }
        //查询用户是否存在
            User queryList = userService.getUserByAccount(username);
            if (queryList != null) {


           /* org.springframework.security.core.userdetails.User authUser = new org.springframework.security.core.userdetails.User(
                    queryList.getAccount(), queryList.getPassword(), list);*/

            return queryList;
        }
        throw new UsernameNotFoundException("用户名不存在");

    }

}
