package org.niugang.coding.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.*;
import lombok.Getter;
import org.niugang.coding.enums.ExceptionEnum;
import org.niugang.coding.exception.ServiceException;
import org.niugang.coding.vo.UserVO;
import org.niugang.coding.dto.UserDTO;
import org.niugang.coding.service.UserService;
import org.niugang.coding.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@RestController
/**
 * 在restFul api中资源是复数
 */
@RequestMapping("users")
/**
 * 启用方法参数校验
 */
@Validated

/**
 * 类说明
 */
@Api(value = "用户接口", tags = {"用户操作访问接口"})
/**
 * 响应状态码说明
 */
@ApiResponses({
        @ApiResponse(code = 200, message = "业务处理成功"),
        @ApiResponse(code = 400, message = "参数错误"),
        @ApiResponse(code = 500, message = "业务处理失败"),
        @ApiResponse(code = 404, message = "地址不存在"),
        @ApiResponse(code = 401, message = "没有认证"),
        @ApiResponse(code = 403, message = "无权限"),
})
/**
 * 用户web层
 *
 * @author Created by niugang on 2018/12/26/16:19
 */
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 查询列表
     *
     * @param userDTO  参数
     * @param pageNo   页码
     * @param pageSize 页码大小 防止数据库查询慢，pageSize支持最大为100
     * @return ResponseEntity<PageResult                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               UserVO>>
     */
    @ApiOperation(value = "获取用户列表", httpMethod = "GET")
    @GetMapping("list")
    public ResponseEntity<PageResult<UserVO>> list(UserDTO userDTO,  @Min(1) @RequestParam(defaultValue = "1") Integer pageNo, @Max(100) @Min(5) @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<UserVO> listByPage = userService.getListByPage(userDTO, pageNo, pageSize);
        PageResult<UserVO> result = new PageResult<>();
        result.setTotal(listByPage.getTotal());
        result.setData(listByPage.getList());
        result.setTotalPage(listByPage.getPages());
        result.setPageNO(pageNo);
        result.setPageSize(pageSize);
        return ResponseEntity.ok(result);
    }

    @ApiOperation(value = "获取用户列表", httpMethod = "GET")
    @GetMapping("finds")
    public ResponseEntity<PageResult<UserVO>> finds(UserDTO userDTO,  @Min(1) @RequestParam(defaultValue = "1") Integer pageNo, @Max(100) @Min(5) @RequestParam(defaultValue = "10") Integer pageSize) {
        PageInfo<UserVO> listByPage = userService.getListByPage(userDTO, pageNo, pageSize);
        PageResult<UserVO> result = new PageResult<>();
        result.setTotal(listByPage.getTotal());
        result.setData(listByPage.getList());
        result.setTotalPage(listByPage.getPages());
        result.setPageNO(pageNo);
        result.setPageSize(pageSize);
        return ResponseEntity.ok(result);
    }

    /**
     * 添加
     * {@code @Valid  @Validated 都可以 }
     *
     * @return ResponseEntity<Void>
     */
    @PostMapping
    @ApiOperation(value = "增加用户", httpMethod = "POST")
    public ResponseEntity<Void> save( @Validated @RequestBody UserDTO userDTO) {
        userService.insert(userDTO);
        return ResponseEntity.ok().build();
    }




}
