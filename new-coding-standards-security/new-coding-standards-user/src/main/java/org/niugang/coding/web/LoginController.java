package org.niugang.coding.web;

import org.niugang.coding.enums.ExceptionEnum;
import org.niugang.coding.exception.ServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Created by niugang on 2019/1/25/17:20
 */
@RestController
public class LoginController {


    @GetMapping("/login_p")
    public ResponseEntity<String> login() {
        throw new ServiceException(ExceptionEnum.UN_LOGIN);
    }
}
