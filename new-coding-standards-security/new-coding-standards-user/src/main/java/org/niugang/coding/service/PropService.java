package org.niugang.coding.service;

import org.niugang.coding.conf.PropConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.DocFlavor;

/**
 * @author Created by niugang on 2019/1/18/15:34
 */
@Service
public class PropService {

    @Autowired
    private PropConfig propConfig;


    public  void print(){
        System.out.println(propConfig.toString());
    }

}
