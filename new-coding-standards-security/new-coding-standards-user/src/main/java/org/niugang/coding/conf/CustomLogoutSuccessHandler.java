package org.niugang.coding.conf;

import com.alibaba.fastjson.JSON;
import org.niugang.coding.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 自定义注销成功
 *
 * @author Created by niugang on 2019/1/25/20:27
 */
@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        httpServletResponse.setContentType("application/json;charset=utf-8");
        CustomException respBean = CustomException.builder().message("注销成功").status(HttpStatus.OK.value()).path(httpServletRequest.getServletPath()).timestamp(System.currentTimeMillis()).build();
        PrintWriter out = httpServletResponse.getWriter();
        out.write(JSON.toJSONString(respBean));
        out.flush();
        out.close();


    }
}
