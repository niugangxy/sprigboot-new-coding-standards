package org.niugang.coding.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.print.DocFlavor;

/**
 * 测试类
 *
 * @author Created by niugang on 2018/12/26/11:58
 */
@Controller
@RequestMapping("test")
@Api(value = "测试接口", tags = {"测试操作接口"})
public class TestController {

    @ApiOperation(value = "项目测试", httpMethod = "GET")
    @GetMapping
    @ResponseBody
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("hello world");
    }


    @GetMapping("/")
    public String index(){
        return "index";
    }

}
