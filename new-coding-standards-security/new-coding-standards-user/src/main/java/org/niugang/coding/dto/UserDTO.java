package org.niugang.coding.dto;

import com.sun.org.apache.bcel.internal.generic.NEW;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * 数据传输对象
 *
 * @author Created by niugang on 2018/12/26/13:43
 */
@ToString
@Data
@EqualsAndHashCode
@ApiModel
public class UserDTO {

    @ApiModelProperty(value = "ID",  notes = "添加不同传id")
    private Integer id;
    /**
     * {@code @NotNull javax.validation 的包 }
     */
    @ApiModelProperty(value = "账号")
    @NotNull
    private String account;
    @ApiModelProperty(value = "姓名")
    @NotNull
    private String username;
    @NotNull
    @ApiModelProperty(value = "性别")
    private String sex;
    @NotNull
    @Pattern(regexp = "^[0-9]{11}$")
    @ApiModelProperty(value = "电话")
    private String phone;
    @NotNull
    @Email
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Valid
    @NotNull
    public   InnerUserDTo innerUserDTo;

    @Data
    private static  class InnerUserDTo{
        @NotEmpty
        private String children;
    }

}
