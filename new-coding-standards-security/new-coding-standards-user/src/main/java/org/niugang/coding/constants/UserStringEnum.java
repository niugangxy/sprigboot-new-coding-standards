package org.niugang.coding.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 字符串类型枚举
 *
 * @author Created by niugang on 2018/12/26/21:25
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum UserStringEnum {

    DELETE_FLAG("1", "删除标识符");
    String value;
    String message;
}
