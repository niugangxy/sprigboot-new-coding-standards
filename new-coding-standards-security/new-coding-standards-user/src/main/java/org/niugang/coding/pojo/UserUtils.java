package org.niugang.coding.pojo;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Created by niugang on 2019/1/25/19:22
 */
public class UserUtils {
    public static User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
