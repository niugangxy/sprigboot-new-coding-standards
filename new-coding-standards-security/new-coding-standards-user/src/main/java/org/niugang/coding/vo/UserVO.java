package org.niugang.coding.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 用户相关显示对象
 *
 * @author Created by niugang on 2018/12/26/16:11
 */
@Data
@ToString
@EqualsAndHashCode
public class UserVO {
    private Integer id;
    private String account;
    private String username;
    private String sex;
    private String phone;
}
