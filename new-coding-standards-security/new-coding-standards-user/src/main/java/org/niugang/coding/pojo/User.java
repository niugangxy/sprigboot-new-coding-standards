package org.niugang.coding.pojo;



import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Created by niugang on 2018/12/26/13:40
 */
@Data
@ToString
@EqualsAndHashCode
public class User implements UserDetails {
    private Integer id;
    private String account;
    private String username;
    private String sex;
    private String phone;
    private String email;
    private String quanPin;
    private String jianPin;
    private String ip;
    private Integer deptId;
    private String roleId;
    private String idCard;
    private String fileUrl;
    @JSONField(serialize = false)
    private String password;
    private String deleteFlag;
    private Date createTime;
    private String creator;
    private Date updateTime;
    private String updator;
    private Long version;


    @Override
    @JSONField(serialize = false)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //   查询用户拥有的角色
        List<GrantedAuthority> list = new ArrayList<>();
        // 如果是admin用户登录，授予SUPERADMIN权限
        ///if (username.equals("admin")) {
        list.add(new SimpleGrantedAuthority("SUPERADMIN"));
        ///	}
        return list;
    }

    @Override
    @JSONField(serialize = false)
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JSONField(serialize = false)
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JSONField(serialize = false)
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JSONField(serialize = false)
    public boolean isEnabled() {
        return true;
    }
}
