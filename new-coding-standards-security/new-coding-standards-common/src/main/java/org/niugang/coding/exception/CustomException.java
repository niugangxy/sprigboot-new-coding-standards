package org.niugang.coding.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;


/**
 * @author Created by niugang on 2019/1/23/11:41
 */
@Data
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomException {
    private int status;
    private String path;
    private String message;
    private long timestamp;
    private Object data;
}
