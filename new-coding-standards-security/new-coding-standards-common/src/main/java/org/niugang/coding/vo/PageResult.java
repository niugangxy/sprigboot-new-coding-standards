package org.niugang.coding.vo;

import lombok.Data;

import java.util.List;

/**
 * 分页结果显示
 *
 * @author Created by niugang on 2018/12/26/12:28
 */
@Data
public class PageResult<T> {

    private Long total;
    private Integer totalPage;
    private List<T> data;
    private Integer pageSize;
    private Integer pageNO;


}
