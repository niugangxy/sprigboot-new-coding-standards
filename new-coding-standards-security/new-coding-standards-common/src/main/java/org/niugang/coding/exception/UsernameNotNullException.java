package org.niugang.coding.exception;

/**
 * 用户名不能为空异常
 *
 * @author Created by niugang on 2019/1/23/13:58
 */
public class UsernameNotNullException extends RuntimeException {
}
