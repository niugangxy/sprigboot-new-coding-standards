package org.niugang.coding.dao;

import java.util.List;


/**
 * 基础dao
 *
 * @param <T> POJO
 * @param <S> DTO
 * @author Created by niugang on 2018/12/26/14:19
 */
public interface BaseDao<T, S> {

    /**
     * 查询列表
     *
     * @param s dto对象
     * @return List<T>
     */
    List<T> getList(S s);

    /**
     * 分页查询
     *
     * @param s dto对象
     * @return List<T>
     */
    List<T> getListByPage(S s);

    /**
     * 插入
     *
     * @param t pojo对象
     */
    void insert(T t);

    /**
     * 查询
     *
     * @param id 主键
     */
    void delete(int id);

    /**
     * 根据id查询
     *
     * @param id 主键
     * @return T
     */
    T getById(int id);

    /**
     * 组合条件查询
     *
     * @param s dto对象
     * @return T
     */
    T get(S s);

    /**
     * 查询数量
     *
     * @param s dto对象
     * @return int
     */
    int count(S s);

    /**
     * 更新
     *
     * @param t pojo
     */
    void update(T t);
}
