package org.niugang.coding.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author Created by niugang on 2018/12/26/12:18
 */
@NoArgsConstructor
@AllArgsConstructor
public enum ExceptionEnum {
    /**
     *
     */
    PARAMS_VALIDATE_FAIL(400, "'参数校验失败"),
    BUSINESS_DEAL_FAIL(500, "'业务处理失败");
    /**
     * 响应状态码
     */
    int value;
    /**
     * 响应描述
     */
    String message;

    public int value() {
        return this.value;
    }

    public String message() {
        return this.message;
    }
}
