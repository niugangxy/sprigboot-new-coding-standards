package org.niugang.coding.service;

import java.util.List;

import com.github.pagehelper.PageInfo;


/**
 * @param <T> 数据传出DTO
 * @param <V> 显示对象VO
 * @author Created by niugang on 2018/12/26/15:19
 */
public interface BaseService<T, V> {

    List<V> getList(T t);

    PageInfo<V> getListByPage(T t, int pageNo, int pageSize);

    void insert(T s);

    void delete(int id);

    V getById(int id);

    V get(T t);

    int count(T t);

    void update(T t);
}
