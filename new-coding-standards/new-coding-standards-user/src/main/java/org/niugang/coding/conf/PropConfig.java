package org.niugang.coding.conf;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author Created by niugang on 2019/1/18/15:32
 */
@Data
@ToString
@ConfigurationProperties(prefix = "spring.prop")
@Component
public class PropConfig {
  private String name;
  private String sex;
  private String address;
  private String phone;

}
