package org.niugang.coding;




import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@MapperScan(value={"org.niugang.coding.mapper"})
public class NewCodingStandardsUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewCodingStandardsUserApplication.class, args);
    }



}

