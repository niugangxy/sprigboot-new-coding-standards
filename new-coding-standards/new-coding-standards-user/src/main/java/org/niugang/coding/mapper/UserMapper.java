package org.niugang.coding.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.niugang.coding.dao.BaseDao;
import org.niugang.coding.dto.UserDTO;
import org.niugang.coding.pojo.User;
import org.springframework.stereotype.Component;


/**
 * @author Created by niugang on 2018/12/26/13:40
 */
@Mapper
@Component(value = "userMapper")
public interface UserMapper extends BaseDao<User, UserDTO> {



}
