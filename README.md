# sprigboot-new-coding-standards

#### 介绍
SpringBoot脚手架，遵守严格的代码规范。

##### 规范详细说明

1.遵循标准RESTful API

2.异常采用枚举进行封装(业务内部异常往上抛，返回客户端需要将对应的异常转换为具体的状态的，这里介绍Spring提供的全局异常处理)
```java
package org.niugang.coding.advice;

import lombok.extern.slf4j.Slf4j;
import org.niugang.coding.enums.ExceptionEnum;
import org.niugang.coding.exception.ServiceException;
import org.niugang.coding.vo.ExceptionResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

/**
 * 全部异常处理
 *
 * @author Created by niugang on 2018/12/26/12:19
 */
@ControllerAdvice
@Slf4j
public class BasicExceptionHandler {

    /**
     * 具体业务层异常
     *
     * @param e 业务异常
     * @return ResponseEntity<ExceptionResult>
     */
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<ExceptionResult> handleException(ServiceException e) {

        /**
         * 响应的状态码，为枚举中定义的状态码
         */
        return ResponseEntity.status(e.getExceptionEnum().value())
                .body(new ExceptionResult(e.getExceptionEnum()));
    }


    /**
     * 业务处理未知异常
     *
     * @param e 异常
     * @return ResponseEntity<ExceptionResult>
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exceptionResultResponseEntity(Exception e) {
        //所有参数异常
        //在对象上绑定校验如(UserDTO)
        if (e instanceof BindException || e instanceof MethodArgumentNotValidException || e instanceof IllegalArgumentException) {
            log.error("参数校验失败:{}", e);
            return ResponseEntity.status(ExceptionEnum.PARAMS_VALIDATE_FAIL.value())
                    .body(new ExceptionResult(ExceptionEnum.PARAMS_VALIDATE_FAIL));
        }
        //方法上参数校验失败
        if (e instanceof ConstraintViolationException) {
            ConstraintViolationException ex = (ConstraintViolationException) e;
            Map<String, Object> res = new HashMap<>(16);
            res.put("status", HttpStatus.BAD_REQUEST.value());
            res.put("message", ex.getMessage());
            res.put("timestamp", System.currentTimeMillis());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(res);

        }

        log.error("服务器内部异常:{}", e);
        /*
         * 响应的状态码，为枚举中定义的状态码
         */
        return ResponseEntity.status(ExceptionEnum.BUSINESS_DEAL_FAIL.value())
                .body(new ExceptionResult(ExceptionEnum.BUSINESS_DEAL_FAIL));
    }
}

```


- value 对应响应状态码

- message 错误描述

```java
@NoArgsConstructor
@AllArgsConstructor
public enum ExceptionEnum {
    /**
     *
     */
    PARAMS_VALIDATE_FAIL(400, "'参数校验失败"),
    BUSINESS_DEAL_FAIL(500, "'业务处理失败");
    /**
     * 响应状态码
     */
    int value;
    /**
     * 响应描述
     */
    String message;

    public int value() {
        return this.value;
    }

    public String message() {
        return this.message;
    }
}


```
>参数错误对应400状态码

3.遵循严格的pojo,vo,dto(来自阿里java规范)

4.建议提倡使用lomback(让你的代码,更加简洁，干净)

5.对于日志记录采用@Slf4j

>以前可能是：
```java
private static final Logger logger = LoggerFactory.getLogger(MeetingInfoController.class);
```

6.对于响应采用ResponseEntity,Spring已经封装好的，在一定程度上是能满足业务场景的

```java
 @PostMapping
public ResponseEntity<Void> save(@Valid @RequestBody  UserDTO userDTO) {
        userService.insert(userDTO);
        return ResponseEntity.ok().build();
    }
```

7.RESTful API对应以下Spring请求注解

- @GetMapping 查询

- @PostMapping  新增

- @PutMapping  修改

- @DeleteMapping 删除

8.进行必要的参数校验，新增参数校验，普通查询也有必要，如分页查询pageSize不做限制可能导致数据查询异常或慢查询

9.建议直接只用框架自带封装好的API,如RedisTemplate,RabbitTemplate,KafkaTemplate(自己写的通用的可能某些地方考虑不全)

10.使用注解事务@Transactional
```java
import org.springframework.transaction.annotation.Transactional;
//spring的注解不是java注解
```

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)